import React from "react";
import ProjectCard from "./ProjectCard";

const Projects = () => {
  const projectList = [
    {
      id: 1,
      title: "Project 1",
      description: "A short description of project 1.",
      imageUrl: "https://via.placeholder.com/150",
      repoUrl: "https://github.com/your-username/project1",
    },
    {
      id: 2,
      title: "Project 2",
      description: "A short description of project 2.",
      imageUrl: "https://via.placeholder.com/150",
      repoUrl: "https://github.com/your-username/project2",
    },
  ];

  return (
    <section id="projects" className="py-12 bg-gray-100">
      <div className="container mx-auto px-4">
        <h2 className="text-3xl mb-6">My Projects</h2>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
          {projectList.map((project) => (
            <ProjectCard key={project.id} project={project} />
          ))}
        </div>
      </div>
    </section>
  );
};

export default Projects;
