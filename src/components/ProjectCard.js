import React from "react";

const ProjectCard = ({ project }) => {
  const { title, description, imageUrl, repoUrl } = project;

  return (
    <div className="bg-white shadow-md p-4 rounded-md">
      <div className="flex flex-col h-full">
        <img
          src={imageUrl}
          alt={title}
          className="w-full h-40 object-cover rounded-md mb-4"
        />
        <div className="flex-grow">
          <h3 className="text-xl font-semibold mb-2">{title}</h3>
          <p className="text-gray-600">{description}</p>
        </div>
        <div className="mt-4">
          <a
            href={repoUrl}
            target="_blank"
            rel="noopener noreferrer"
            className="text-blue-600 hover:text-blue-800"
          >
            View on GitHub
          </a>
        </div>
      </div>
    </div>
  );
};

export default ProjectCard;
