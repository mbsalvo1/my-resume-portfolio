import React from "react";

const About = () => {
  return (
    <section id="about" className="py-12 bg-white">
      <div className="container mx-auto px-4">
        <div className="flex flex-wrap">
          <div className="w-full md:w-1/2 md:pr-4">
            <h2 className="text-3xl mb-4">About Me</h2>
            <p className="mb-4">
              I'm a passionate developer with experience in XYZ technologies. I
              enjoy building web applications and learning new technologies.
            </p>
            <a
              href="https://www.linkedin.com/in/your-profile"
              className="text-blue-600 hover:text-blue-800"
            >
              Check my LinkedIn profile
            </a>
          </div>
          <div className="w-full md:w-1/2 md:pl-4">
            {/* Replace with your image URL */}
            <img
              src="https://via.placeholder.com/300"
              alt="Your Name"
              className="w-full h-auto"
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
